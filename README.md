#Removing compatibility issues

1) Delete any previous installation of MacPorts since this will
interfere with Homebrew.

2) Delete any other versions of Python except the original pre
installed version. This is likely to be found with the following
path:

```
/usr/bin/python
```

#Installing Homebrew

3) In a Terminal, install [Homebrew](http://brew.sh) using the
following command:

```
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

4) Run `brew doctor` to diagnose any problems with MacPorts or
conflicting versions of Python.  Instructions are provided that should
be followed, for example updating or moving files.

5) Run `brew update` to check if new files need updating.

6) Install any newly added homebrew updates using `brew install`.

7) Tap into [Homebrew's scientific formula packages](https://github.com/Homebrew/homebrew-science):
```
brew tap homebrew/science 
```

#Installing dependicies

8) Install the following list of dependencies using Homebrew:

```
brew install git mpich2 boost eigen cppunit pkgconfig swig cmake python vtk
```

9) Reinstall HDF5 (which was automatically installed as part of the
VTK installation in the previous step) with parallel support:

```
brew reinstall hdf5 --with-mpi
```

10) Make sure that the Homebrew version of Python is the default by 
adding `/usr/local/bin` to your `PATH`. This can be accomplished 
by adding the following line to $HOME/.profile:

```
export PATH=/usr/local/bin:$PATH 
```

Also copy this line into your current terminal so that it takes
immediate effect.

```
which python
```

This should say `/usr/local/bin/python`.

11) Install the following list of Python dependencies using pip:

```
pip install numpy sympy six ply ipython sphinx pytest
```

12) Download, configure, build and install PETSc:

* Download PETSc from [bitbucket](https://bitbucket.org/petsc/petsc):

```
mkdir -p $HOME/local/src
cd $HOME/local/src
git clone git@bitbucket.org:petsc/petsc.git
```

* Configure PETSc:

```
cd petsc
./configure --prefix=$HOME/local —download-ml --download-superlu_dist --download-parmetis --download-metis --download-ptscotch --download-mumps --download-scalapack --download-hypre --with-debugging=no COPTFLAGS=-Ofast FOPTFLAGS=-Ofast CXXOPTFLAGS=-Ofast --download-superlu --download-suitesparse --download-superlu_dist
```

* Build and install PETSc:
```
make PETSC_DIR=$HOME/local/src/petsc PETSC_ARCH=arch-darwin-c-opt all
make PETSC_DIR=$HOME/local/src/petsc PETSC_ARCH=arch-darwin-c-opt install
```

#Downloading and installing FEniCS

13) Download the following FEniCS packages:

```
cd $HOME/local/src
git clone git@bitbucket.org:fenics-project/dijitso.git
git clone git@bitbucket.org:fenics-project/instant.git
git clone git@bitbucket.org:fenics-project/fiat.git
git clone git@bitbucket.org:fenics-project/ufl.git
git clone git@bitbucket.org:fenics-project/ffc.git
git clone git@bitbucket.org:fenics-project/dolfin.git
```

If the most up to date developer version is required, periodically
update the clones by selecting the correct directory in a terminal and
run `git pull` and then `git log` to check what has changed.
 
14) Build and install the following FEniCS packages using distutils:

```
cd dijitso   && python setup.py install --prefix=$HOME/local/fenics && cd ..
cd instant   && python setup.py install --prefix=$HOME/local/fenics && cd ..
cd fiat      && python setup.py install --prefix=$HOME/local/fenics && cd ..
cd ufl       && python setup.py install --prefix=$HOME/local/fenics && cd ..
cd ffc       && python setup.py install --prefix=$HOME/local/fenics && cd ..
```

15) Update your environment so that the final installation of DOLFIN
below will find your installed packages by adding the following
lines to $HOME/.profile:

```
export CMAKE_PREFIX_PATH=$HOME/local
export PATH=$CMAKE_PREFIX_PATH/bin:$PATH
export PYTHONPATH=$CMAKE_PREFIX_PATH/lib/python2.7/site-packages:$PYTHONPATH
```

Also copy these lines into your current terminal so that they take 
immediate effect.

16) Build the FEniCS DOLFIN package using CMake:

```
mkdir build
cd build
cmake ..
make install
```

17) Finally, you need to source the generated file `build/dolfin.conf`
in the DOLFIN source directory. Add the following line to
$HOME/.profile or just source it in the terminal:

```
source $HOME/local/src/dolfin/build/dolfin.conf
```